# Toggl2Redmine
![](images/image.png)

Toggl2Redmine is CLI utility to sync time entries to Redmine.
This should be used together with 
* [Toggl Track Plugin for Google Chrome](https://chrome.google.com/webstore/detail/toggl-track-productivity/oejgccbfbmkkpaidnkphaiaecficdnfn) or
* [Toggl Track Plugin for Firefox](https://addons.mozilla.org/en-US/firefox/addon/toggl-button-time-tracker)

Basically if you have `#[issue id]` in description of time entry in Toggl and tag (see bellow) you will be able to sync it to Redmine. 

But it is better to use plugins above. If you set it correctly you will get nice time entries. For more info see [Redmine Time Tracking](https://toggl.com/track/redmine-time-tracking/).
## Instalation
### Using Composer
```shell script
composer global require stejk/toggl2redmine
```

### Using wget
```shell script
wget https://gitlab.com/jakub.stejskal2/toggl2redmine/-/raw/master/builds/toggl2redmine -O toggl2redmine
chmod a+x ./toggl2redmine
mv ./toggl2redmine /usr/local/bin/toggl2redmine
```

Run toggl2redmine and choose first option `Set Toggl2Redmine` and setup your API keys, and tag.

## Usage
* Run `toggl2redmine` and choose second option `Sync Toggl to Redmine`.
* Choose date which you want to sync entries from. (Default: `Today - 1 day`)
* Choose date which you want to sync entries to. (Default: `Today + 1 day`)
