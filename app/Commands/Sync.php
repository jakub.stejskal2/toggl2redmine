<?php

namespace App\Commands;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class Sync extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'sync';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function handle()
    {
        $tag = Storage::get(".tag.txt");

        $from = $this->ask("From", Carbon::now()->subDay()->format('Y-m-d'));
        $to = $this->ask("To", Carbon::now()->addDay()->format('Y-m-d'));

        $entries = $this->getTogglEntries(
            Carbon::parse($from),
            Carbon::parse($to)
        );

        $entries = $entries
            ->filter(static function (array $entry) use ($tag) {
                return array_key_exists('tags', $entry) && in_array($tag, $entry['tags'], true);
            })
            ->filter(static function (array $entry) {
                return Str::contains($entry['description'], '#');
            })
            ->filter(static function (array $entry) {
                return $entry['duration'] > 0;
            });

        $this->info("Syncing {$entries->count()} time entries" . PHP_EOL);
        $bar = $this->output->createProgressBar($entries->count());

        $bar->start();

        $results = $entries
            ->map(function (array $entry) use ($bar) {
                $issueId = (int) Str::before(Str::after($entry['description'], '#'), ' ');
                $day = Carbon::parse($entry['start']);
                $duration = ((int) $entry['duration']) / (60 * 60);

                $bar->advance();

                return $this->addEntryToRedmine(
                    $issueId,
                    $day,
                    $duration
                );
            })
            ->filter(static function (bool $result) {
                return $result;
            });

        $bar->finish();
        $this->info(PHP_EOL . PHP_EOL . "Synced {$results->count()} time entries");
    }

    private function getTogglEntries(Carbon $from, Carbon $to): Collection
    {
        $apiKeyToggl = trim(Storage::get(".api_key_toggl.txt"));

        $response = Http::withBasicAuth(
            $apiKeyToggl,
            'api_token'
        )->get('https://api.track.toggl.com/api/v8/time_entries', [
            'start_date' => $from->toIso8601String(),
            'end_date'   => $to->toIso8601String(),
        ]);

        $entries = $response->json();

        return collect($entries);
    }

    private function addEntryToRedmine(int $issueId, Carbon $day, float $hours): bool
    {
        $url = Storage::get(".url.txt");
        $apiKey = Storage::get(".api_key.txt");

        $requestRedmine = [
            'key'        => $apiKey,
            "time_entry" => [
                "issue_id" => $issueId,
                "spent_on" => $day->format('Y-m-d'),
                "hours"    => $hours,
            ],
        ];

        try {
            $response = Http::post("{$url}/time_entries.json", $requestRedmine);

            $response = $response->toPsrResponse();

            if (in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED], true)) {
                $this->info("Successfully synced [#{$issueId}]!");

                return true;
            }

            $this->error("Failed to sync [#{$issueId}]! Exception: {$response->getBody()->getContents()}");
        } catch (\Throwable $e) {
            $this->error("Failed to sync [#{$issueId}]! Exception: {$e->getMessage()}");
        }

        return false;
    }
}
