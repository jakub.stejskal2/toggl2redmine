<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class Login extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'setup';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Setup command data';

    public function handle()
    {
        $url = $this->ask("Fill URL of Redmine", "https://redmine.com");
        Storage::put(".url.txt", $url);

        $apiKey = $this->ask("Fill your API access key from {$url}/my/account");
        Storage::put(".api_key.txt", $apiKey);

        $apiKeyToggl = $this->ask("Fill your API access key from https://track.toggl.com/profile");
        Storage::put(".api_key_toggl.txt", $apiKeyToggl);

        $url = Storage::get(".url.txt");
        $apiKey = Storage::get(".api_key.txt");
        $apiKeyToggl = Storage::get(".api_key_toggl.txt");

        $this->table(
            ["URL", "API Key"],
            [
                [$url, $apiKey],
                ['https://api.track.toggl.com', $apiKeyToggl],
            ]
        );

        $tag = $this->ask("Which tag you want to sync", "redmine");
        Storage::put(".tag.txt", $tag);

        $this->info("Successfully set command!");
    }

}
