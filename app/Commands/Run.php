<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

/**
 * @method menu(string $string, string[] $array)
 */
class Run extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'run';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Run Toggl2Redmine';

    public function handle()
    {
        $option = $this->menu('What do you want?', [
            'Set Toggl2Redmine',
            'Sync Toggl to Redmine',
        ])->open();

        switch ($option) {
            case 0:
                $this->call(Login::class);
                break;
            case 1:
                $this->call(Sync::class);
                break;
            default:
                $this->error("Not choosed!");
                break;
        }
    }
}
